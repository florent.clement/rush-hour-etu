#ifndef PLATEAU_H
#define PLATEAU_H

#include <case.h>
#include <element.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <functional>
#include <sstream>
#include <string>
#include <unordered_set>

struct Mouvement{
    unsigned int m_indexElement;
    Direction m_direction;
    unsigned int m_longueur;

    explicit Mouvement(){};
    Mouvement(int indexElement, Direction direction, unsigned int longueur)
              : m_indexElement(indexElement),
                m_direction(direction),
                m_longueur(longueur){};
};

/*
 * Classe représentant le plateau/puzzle avec des vehicules permettant
 * toutes les intéractions avec ces derniers et la résolution du problème
 */
class Plateau
{

public:

    explicit Plateau(int largeur = 6, int hauteur = 6);

    //Permet de construire le plateau à partir du chemin vers un fichier
    void chargerPuzzle(const char* cheminFichier);

    //Déplace un vehicule du plateau dans la direction et la longueur souhaitées
    void deplacerElement(int index, Direction direction, unsigned int longueur);
    bool operator== (const Plateau plateau) const;

    //Resolution de ce plateau
    //Renvoie les coups permettant de sortir la voiture
    std::vector<Mouvement> resoudre();

    //Resolution du plateau entré en paramètre
    //Renvoie les coups permettant de sortir la voiture
    static std::vector<Mouvement> resoudre(const Plateau src);
    //Generation d'un puzzle
    void generate();

    void setSortie(int hauteur, int largeur);

    //Getters des attributs
    int getIndexVehiculeASortir();
    Element getElementASortir();
    const int getLargeur() const { return this->m_largeur; }
    const int getHauteur() const { return this->m_hauteur; }
    Element* getElementByCase(Case src);
    const std::vector<Element> getElement() const { return this->lElement; }

private:
    //Test l'existance d'un plateau dans la liste de plateau lPlateau
    static bool plateauExiste(std::unordered_set<std::string > lPlateau,
                              std::string stringPlateau);

    /*
      Fonctions permettant de faire la correspondance entre l'orientation
      des vehicules en type Direction et celle en type int
    */
    Direction intToDirection(int orientation);
    int directionToInt(Direction dir);
    //Retourne vrai quand la case est utilisée par aucun Element
    bool deplacerEstPossible(Case src);
    //Heuristique calculant la distance entre le vehicule cible et la sortie
    int distanceSortie() const;
    //Heuristique calculant la distance entre un vehicule quelconque et la sortie
    int distanceSortie(Element element) const;
    //Renvoie la liste de tout les mouvements possibles
    //autour du vehicule entré en paramètre
    std::vector<struct Mouvement> getListeDesMouvementPossible(int indexElement);
    //Verifie si un element est insérable
    bool estInserable(Element element);

    //Clone le plateau
    Plateau clonePlateauAvecMouvement(Mouvement mouvement);
    //Convertion du plateau en chaine de caractères
    std::string to_str() const;

    //Largeur du plateau
    int m_largeur;
    //Hauteur du plateau
    int m_hauteur;
    //Positionnement de la porte
    Case sortie;
    //Liste de tout les éléments présents dans le plateau
    std::vector<Element> lElement;
    //Liste de mouvements
    std::vector<Mouvement> m_listeMouvement;
    //Vehicule à sortir pour résoudre le puzzle
    int m_indexElementASortir;
};

//Gestion de l'affichage
std::ostream& operator<<(std::ostream&, const Plateau&);

#endif // PLATEAU_H
