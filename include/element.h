#ifndef ELEMENT_H
#define ELEMENT_H

#include <case.h>

enum Direction {
    NORD,
    SUD,
    EST,
    OUEST
};

/*
 * Classe contenant les propriétés d'un vehicule unique
 */
class Element
{

public:

    explicit Element();

    Element(Case pos, Direction direction, unsigned int longueur);
    Element(Element const& element);

    /**
    * Déplace un élément (vehicule) dans la direction et la longueur indiquée
    * @param m_direction
    */
    void deplacer(Direction direction, unsigned int longueur);

    /**
    * Vérifie si une case est occupée
    * @param m_pos
    * @return VRAI si la case est déjà occupée par un élément
    */
    bool estOccupee(Case pos);

    /**
    * Retourne la première cellule sur une direction donnée

    * Par exemple si la direction NORD est donnée,
    * la cellule la plus au NORD sera renvoyée
    * @param direction
    */
    Case getFirstCase(Direction direction);

    /**
    * Retourne la dernière cellule sur une direction donnée
    
    * Par exemple si la direction NORD est donnée,
    * la cellule la plus au SUD sera renvoyée
    * @param direction
    */
    Case getLastCase(Direction direction);

    //Getters des attributs
    const Case getCase() const { return this->m_pos; }
    const int getLongueur() const { return this->m_longueur; }
    const Direction getDirection() const { return this->m_direction; }

    /**
    * Renvoie la direction opposée à celle passée en paramètre
    * Exemple: passer NORD renverra SUD
    * @param m_direction
    * @return
    */
    Direction getDirectionOpposee(Direction m_direction);

    bool operator== (const Element element) const;

private:

    /**
    * Test l'opposition de deux directions
    * @param directionA
    * @param directionB
    * @return vrai si les directions sont effectivement opposées
    */
    bool directionIsOpposed(Direction directionA, Direction directionB);

    //Position actuelle du vehicule dans le plateau
    Case m_pos;
    //Direction du vehicule sur les positions NORD, SUD, EST et OUEST
    Direction m_direction;
    //Longueur du vehicule entre 2 et 3
    int m_longueur;
};

#endif // ELEMENT_H
