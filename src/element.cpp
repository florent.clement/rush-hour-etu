#include <element.h>

Element::Element() = default;

Element::Element(Case pos, Direction direction, unsigned int longueur):
                m_pos(pos), m_direction(direction), m_longueur(longueur) {}

Element::Element(Element const &element)
                : m_pos(element.m_pos), m_longueur(element.m_longueur),
                m_direction(element.m_direction) {}


void Element::deplacer(Direction direction, unsigned int longueur) {
  if(this->m_direction == direction ||
  this->getDirectionOpposee(direction) == this->m_direction){
    switch(direction){
      case NORD:
        this->m_pos.setLigne(this->m_pos.getLigne() - longueur);
        break;
      case SUD:
        this->m_pos.setLigne(this->m_pos.getLigne() + longueur);
        break;
      case EST:
        this->m_pos.setColonne(this->m_pos.getColonne() + longueur);
        break;
      case OUEST:
        this->m_pos.setColonne(this->m_pos.getColonne() - longueur);
        break;
    }
  }
}

bool Element::estOccupee(Case pos) {
  switch(this->m_direction){
    case NORD:
      for(int x = this->m_pos.getLigne(); x < this->m_pos.getLigne() + this->m_longueur; x++){
        if(pos.getLigne() == x && pos.getColonne() == this->m_pos.getColonne())
          return true;
      }
      break;
    case SUD:
      for(int x = this->m_pos.getLigne();x > this->m_pos.getLigne() - this->m_longueur;x--){
        if(pos.getLigne() == x && pos.getColonne() == this->m_pos.getColonne())
          return true;
      }
      break;
    case EST:
      for(int y = this->m_pos.getColonne();y > this->m_pos.getColonne() - this->m_longueur;y--){
        if(pos.getLigne() == this->m_pos.getLigne() && pos.getColonne() == y)
          return true;
      }
      break;
    case OUEST:
      for(int y = this->m_pos.getColonne();y < this->m_pos.getColonne() + this->m_longueur;y++){
        if(pos.getLigne() == this->m_pos.getLigne() && pos.getColonne() == y)
          return true;
        }
      break;
  }
  return false;
}

Case Element::getFirstCase(Direction direction) {
    return directionIsOpposed(this->m_direction, direction)
                            ? this->getLastCase(this->m_direction) : this->m_pos;
}

Case Element::getLastCase(Direction direction) {
    if(directionIsOpposed(this->m_direction, direction))
        return this->m_pos;
    else if(this->m_direction == direction){
        switch(this->m_direction){
            case NORD:return {this->m_pos.getLigne() + this->m_longueur - 1,this->m_pos.getColonne()};
            case SUD: return {this->m_pos.getLigne() - this->m_longueur - 1,this->m_pos.getColonne()};
            case EST: return {this->m_pos.getLigne(),this->m_pos.getColonne() - this->m_longueur - 1};
            case OUEST: return {this->m_pos.getLigne(),this->m_pos.getColonne() + this->m_longueur - 1};
        }
    }
    return this->m_pos;
}

bool Element::directionIsOpposed(Direction directionA, Direction directionB) {
    return this->getDirectionOpposee(directionA) == directionB;
}

Direction Element::getDirectionOpposee(Direction direction) {
    switch(direction){
        case NORD:
            return SUD;
        case SUD:
            return NORD;
        case EST:
            return OUEST;
        case OUEST:
            return EST;
    }
    return direction; // CAS IMPOSSIBLE
}

bool Element::operator==(const Element element) const {
    if(element.m_direction != this->m_direction)
        return false;
    else if(this->m_longueur != element.m_longueur)
        return false;
    return this->m_pos == element.m_pos;
}
